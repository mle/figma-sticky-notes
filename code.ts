figma.showUI(__html__, { visible: false });
figma.ui.resize(0,0);

const COLOR_YELLOW = { r: 1, g: 0.9, b: 0.65 };
const COLOR_BLUE = { r: 0.63, g: 0.84, b: 1 };
const COLOR_RED= { r: 1, g: 0.53, b: 0.53 };
const COLOR_GREEN = { r: 0.65, g: 0.92, b: 0.54 };
const COLOR_PURPLE = { r: 0.71, g: 0.63, b: 0.9 };
const COLOR_ORANGE = { r: 1, g: 0.65, b: 0.45 };
const COLOR_WHITE = { r: 1, g: 1, b: 1 };
const COLOR_CHARCOAL = { r: 0.133, g: 0.133, b: 0.133 };

const DEFAULT_FONT = { family: 'Marker Felt', style: 'Thin' };

let CURRENT_FONT = DEFAULT_FONT;

const SEPARATOR_NEWLINE = "\n";

const BUFFER: number = 16;

interface NoteTheme {
  color: any,
  emoji: string
}

interface Font {
  family: string,
  style: string
}

const notes = {
  yellow: { color: COLOR_YELLOW, emoji: '🟨' },
  blue: { color: COLOR_BLUE, emoji: '🟦' },
  red: { color: COLOR_RED, emoji: '🟥' },
  green: { color: COLOR_GREEN, emoji: '🟩' },
  purple: { color: COLOR_PURPLE, emoji: '🟪' },
  orange: { color: COLOR_ORANGE, emoji: '🟧' },
  white: { color: COLOR_WHITE, emoji: '◻️' },
}

async function loadNoteFont(): Promise<Font> {
  return figma.clientStorage.getAsync('STICKY-NOTES-FONT').then(value => {
    if (value) {
      return JSON.parse(value);
    }
    return DEFAULT_FONT;
  })
}

async function createNote(
  noteColor: NoteTheme,
  noteText: string = 'Text',
  x: number = figma.viewport.center.x,
  y: number = figma.viewport.center.y
  )  {

  const nodes: SceneNode[] = [];

  const textFont = await loadNoteFont();

  await figma.loadFontAsync(textFont);

  const background = figma.createRectangle();
  background.name = 'Background';
  background.cornerRadius = 4;
  background.resize(180, 180);
  background.fills = [{type: 'SOLID', color: noteColor.color}];
  background.effects = [{
    type: 'DROP_SHADOW',
    color: { r: 0, g: 0, b:0, a: 0.25 },
    offset: { x: 0, y: 4},
    radius: 8,
    visible: true,
    blendMode: 'NORMAL'
  }];

  background.resize(180, 180);
  nodes.push(background);

  const text = figma.createText();
  text.fontName = textFont;
  text.characters = noteText;
  text.fontSize = 16;
  text.textAlignHorizontal = 'CENTER';
  text.textAlignVertical = 'CENTER';
  text.textAutoResize = 'WIDTH_AND_HEIGHT';
  text.fills = [{type: 'SOLID', color: COLOR_CHARCOAL }];

  text.x = 8;
  text.y = 8;
  text.resize(164, 164);
  nodes.push(text);

  const group = figma.group(nodes, figma.currentPage);
  group.name = `${noteColor.emoji}  Note`;
  group.x = x;
  group.y = y;

  figma.currentPage.selection = [ group ];

  return group;
}

function pasteText(){
  figma.ui.postMessage({ type: 'paste', paste : true })
}

function insertText(selection, text){
  var textObjectLength = 0;
  if (selection.length){
    for (let i = 0; i < selection.length; i++) {
      if(selection[i].type == 'TEXT'){
        updateText(selection[i], text);
        textObjectLength++
      } else if (selection[i].type == 'GROUP' || selection[i].type == 'FRAME' || selection[i].type == 'COMPONENT' || selection[i].type == 'INSTANCE'){
        insertText(selection[i].children, text);
      }
    }
    if (textObjectLength == 0){
      createNewText(text, selection[0]);
      textObjectLength++
    }
  }else{
    createNewText(text, null);
    textObjectLength++;
  }
  return textObjectLength;
}

function isValidNote(selection) {
  if (selection.length && selection.length === 1) {
    const currentNode = selection[0];
    if (currentNode.type == 'GROUP' || currentNode.type == 'FRAME' || currentNode.type == 'COMPONENT' || currentNode.type == 'INSTANCE') {
      const nodeName = currentNode.name;
      if (nodeName.includes('🟨')
      || nodeName.includes('🟦')
      || nodeName.includes('🟥')
      || nodeName.includes('🟪')
      || nodeName.includes('🟧')
      || nodeName.includes('◻️')){
        return true;
      }
    }
  }
  return false;
}

function getNoteColour(name) {
  if (name.includes('🟨')) {
    return notes.yellow;
  } else if (name.includes('🟦')){
    return notes.blue;
  } else if (name.includes('🟥')){
    return notes.red;
  } else if (name.includes('🟪')){
    return notes.purple;
  } else if (name.includes('🟧')){
    return notes.orange;
  } else if (name.includes('◻️')){
    return notes.white;
  }
}

async function createMultipleNotes(lines, noteColor, startX, startY){
  var offsetY = startY;
  for (var i = 0; i < lines.length; i++) {
    // only create a note if not empty
    if (lines[i].length > 0) {
      await createNote(noteColor, lines[i], startX, offsetY)
        .then(note => offsetY = note.y + note.height + BUFFER);
    }
  }
}

async function paste(selection, lines){
  var selectionCount = selection.length;
  var lineCount = lines.length;

  var center = figma.viewport.center;

  // No selection and single line
  // Action: Add line to yellow note
  if (selectionCount === 0 && lineCount === 1) {
    // only create a note if not empty
    if (lines[0].length > 0) {
      createNote(notes.yellow, lines[0], center.x, center.y);
    }
  }

  // No selection and multiple lines
  // Action: Add multiple yellow
  if (selectionCount === 0 && lineCount > 1) {
    await createMultipleNotes(lines, notes.yellow, center.x, center.y);
  }

  // Single selection and single line
  // Action: Replace text with line
  if (selectionCount === 1 && lineCount === 1) {
    if (isValidNote(selection)) {
      insertText(selection, lines[0]);
    } else {
      createNote(notes.yellow, lines[0]);
    }
  }

  // Single selection and multiple lines
  // Action: Add multiple with same colour as selected note
  if (selectionCount === 1 && lineCount > 1) {
    if (isValidNote(selection)) {
      const noteColor = getNoteColour(selection[0].name);
      const startX = selection[0].x;
      const startY = selection[0].y + selection[0].height + BUFFER;
      await createMultipleNotes(lines, noteColor, startX, startY);
    } else {
      await createMultipleNotes(lines, notes.yellow, center.x, center.y);
    }
  }

  // Multiple selection and single line
  // Action: Replace line in all notes
  if (selectionCount > 1 && lineCount === 1) {
    var valid = true;

    for (var i=0; i < selection.length; i++){
      if (!isValidNote(selection[i])){
        valid = false;
        break;
      }
    }

    if (valid) {
      insertText(selection, lines[0]);
    } else {
      figma.closePlugin('Pasting works on existing notes or when nothing is selected');
    }
  }

  // Multiple selection and multiple line
  // Action: Not sure what you want to do
  if (selectionCount > 1 && lineCount > 1) {
    figma.closePlugin('Try select a single note or removing newlines from text');
  }

  figma.closePlugin();
}

async function createNewText(characters, nodeObject) {
  const textFont = await loadNoteFont();

  await figma.loadFontAsync(textFont);
  const newTextNode = figma.createText()
  newTextNode.fontSize = Number(16);
  newTextNode.fontName = textFont;
  newTextNode.characters = characters
  if(nodeObject){
    newTextNode.x = nodeObject.x + (nodeObject.width / 2) - (newTextNode.width / 2)
    newTextNode.y = nodeObject.y + (nodeObject.height / 2) - (newTextNode.height / 2)
  }else{
    newTextNode.x = figma.viewport.center.x - (newTextNode.width / 2)
    newTextNode.y = figma.viewport.center.y - (newTextNode.height / 2)
  }
  figma.currentPage.appendChild(newTextNode)
  figma.currentPage.selection = [newTextNode]
  return newTextNode;
}

async function updateText(selectedItem, pasteValue) {
  let selectedItemFontName = selectedItem.getRangeFontName(0, 1)
  let textStyleId = selectedItem.getRangeTextStyleId(0, 1)
  await figma.loadFontAsync({ family: selectedItemFontName.family, style: selectedItemFontName.style })
  if(selectedItem.fontName == figma.mixed){
    selectedItem.setRangeFontName(0, selectedItem.characters.length, selectedItemFontName)
  }

  if(textStyleId){
    selectedItem.setRangeTextStyleId(0, selectedItem.characters.length, textStyleId)
  }else{
    selectedItem.setRangeFontSize(0, selectedItem.characters.length, selectedItem.getRangeFontSize(0, 1))
    selectedItem.setRangeTextCase(0, selectedItem.characters.length, selectedItem.getRangeTextCase(0, 1))
    selectedItem.setRangeTextDecoration(0, selectedItem.characters.length, selectedItem.getRangeTextDecoration(0, 1))
    selectedItem.setRangeLetterSpacing(0, selectedItem.characters.length, selectedItem.getRangeLetterSpacing(0, 1))
    selectedItem.setRangeLineHeight(0, selectedItem.characters.length, selectedItem.getRangeLineHeight(0, 1))
  }

  if(selectedItem.getRangeFillStyleId(0, 1)){
    selectedItem.setRangeFillStyleId(0, selectedItem.characters.length, selectedItem.getRangeFillStyleId(0, 1))
  }else{
    selectedItem.setRangeFills(0, selectedItem.characters.length, selectedItem.getRangeFills(0, 1))
  }
  selectedItem.characters = pasteValue
}

function processText(text: string): string[] {
  return text.split(SEPARATOR_NEWLINE);
}

function changeNoteColor(selectedNote: SceneNode, noteColor: NoteTheme) {
  if (selectedNote.type === 'GROUP') {
    selectedNote.name = `${noteColor.emoji}  Note`;
    const background = selectedNote.findOne(n => n.name === "Background");
    if (background && background.type === 'RECTANGLE') {
      background.fills = [{type: 'SOLID', color: noteColor.color}];
    }
  }
}

async function restoreDefaultSettings() {
  CURRENT_FONT = DEFAULT_FONT;
  figma.clientStorage.setAsync('STICKY-NOTES-FONT', JSON.stringify(CURRENT_FONT)).then(x => {
    figma.closePlugin(`Sticky Notes settings restored`);
  }).catch(x => {
    figma.closePlugin(`Issue restoring settings`);
  });
}

async function changeColor(noteColor: NoteTheme) {
  const selection = figma.currentPage.selection;

  // if empty selection, don't do anything
  if (selection.length <= 0) {
    figma.closePlugin('One or more notes need to be selected');
    return;
  }

  for (var i=0; i < selection.length; i++){
    if (isValidNote([selection[i]]) && selection[i].type === 'GROUP'){
      // if valid note then change the background
      changeNoteColor(selection[i], noteColor);
    }
  }
}

figma.ui.onmessage = message => {
  if (message.type === 'paste'){
    if(message.value == null){
      figma.closePlugin('🤔Copy text to clipboard before pasting');
    } else {
      var lines = processText(message.value);
      paste(figma.currentPage.selection, lines);
    }
  }
  if (message.type === 'selectFont') {
    CURRENT_FONT = message.value;
    figma.clientStorage.setAsync('STICKY-NOTES-FONT', JSON.stringify(CURRENT_FONT)).then(x => {
      figma.closePlugin(`Sticky Notes font is now ${CURRENT_FONT.family} ${CURRENT_FONT.style}`);
    }).catch(x => {
      figma.closePlugin(`Issue setting font`);
    });
  }
}

async function main() {
  switch (figma.command) {
    case "yellowNote":{
      await createNote(notes.yellow);
      figma.closePlugin();
      break;
    }
    case "blueNote":{
      await createNote(notes.blue);
      figma.closePlugin();
      break;
    }
    case "redNote":{
      await createNote(notes.red);
      figma.closePlugin();
      break;
    }
    case "greenNote":{
      await createNote(notes.green);
      figma.closePlugin();
      break;
    }
    case "purpleNote":{
      await createNote(notes.purple);
      figma.closePlugin();
      break;
    }
    case "orangeNote":{
      await createNote(notes.orange);
      figma.closePlugin();
      break;
    }
    case "whiteNote":{
      await createNote(notes.white);
      figma.closePlugin();
      break;
    }
    case "changeYellow": {
      await changeColor(notes.yellow);
      figma.closePlugin();
      break;
    }
    case "changeBlue": {
      await changeColor(notes.blue);
      figma.closePlugin();
      break;
    }
    case "changeRed": {
      await changeColor(notes.red);
      figma.closePlugin();
      break;
    }
    case "changeGreen": {
      await changeColor(notes.green);
      figma.closePlugin();
      break;
    }
    case "changePurple": {
      await changeColor(notes.purple);
      figma.closePlugin();
      break;
    }
    case "changeOrange": {
      await changeColor(notes.orange);
      figma.closePlugin();
      break;
    }
    case "changeWhite": {
      await changeColor(notes.white);
      figma.closePlugin();
      break;
    }
    case "pasteText":{
      figma.ui.show();
      pasteText();
      break;
    }
    case "changeFont": {
      // Load current fonts;
      const textFont = await loadNoteFont();
      figma.listAvailableFontsAsync().then(fonts => {
        figma.showUI(__html__, { visible: true });
        
        // Set the current font
        figma.ui.postMessage({ type: 'setFont', currentFont: textFont, fonts: fonts });
        figma.ui.resize(300, 50);
      }).catch(error => {
        figma.notify('There was an error while loading the font :[  Please restart the plugin');
        console.log(error);
      });
      break;
    }
    case "restoreDefault": {
      restoreDefaultSettings();
      figma.closePlugin();
    }  
    default:
      break;
  }
}

main();
