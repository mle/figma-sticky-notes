# Sticky Notes for Figma
Leave notes in your Figma file

You can insert notes one at time by selecting a note colour.

When you run the plugin you can add notes in the following colours: green, red, purple, blue, white, and classic yellow!

Install it for Figma
https://www.figma.com/c/plugin/776702826923617343/Sticky-Notes

## Pasting into notes
If you have text that you would like to paste into your file just select "Paste into note" and your text will be added in a yellow note.

Multiple notes can be created when you paste text that has line breaks in it. This makes adding a collection of notes faster from your favourite text editor and/or spreadsheet.

You can also replace text of an existing note by pasting text when you have that note selected. This also works for inserting multiple notes of a specific colour.

## Developer notes
Using Visual Studio Code you can run the commands `tsc watch` and `tsc build` to run the code. 

More details https://www.figma.com/plugin-docs/setup/
